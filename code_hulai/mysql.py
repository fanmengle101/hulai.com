import pymysql


class MysqlUtil:
    # 初始化函数
    def __init__(self, host, port, user, pwd, db, charset):
        # 连接mysql数据库
        self.conn = pymysql.connect(host=host, port=port, user=user, password=pwd, database=db, charset=charset)
        # 设置自动提交
        self.conn.autocommit(True)
        # 获取游标对象
        self.cursor = self.conn.cursor()

    # 查询 select * from user
    def query(self, sql, params=None):
        # 执行sql
        self.cursor.execute(sql, params)
        # 获取查询的所有数据
        return self.cursor.fetchall()

    # 增删改---update更新数据库函数
    def update(self, sql, params=None):

        try:
            # 执行sql语句
            self.cursor.execute(sql, params)
            # 提交
            self.conn.commit()
        except Exception as result:
            print(result)
            # 回滚
            self.conn.rollback()

    # 关闭函数
    def close(self):
        # 关闭游标
        self.cursor.close()
        # 关闭连接
        self.conn.close()


if __name__ == '__main__':
    mysql1 = MysqlUtil(host='121.40.162.87', port=3306, user='lezai', pwd='lezai123@', db='test1', charset='utf8')
    mysql1.update("INSERT INTO stu values(2,'李四','20','男','2018-12-30','33.10')")
    mysql1.update("delete from stu where id=2")
    a = mysql1.query(sql='select * from stu')
    print(a)
    mysql1.close()
