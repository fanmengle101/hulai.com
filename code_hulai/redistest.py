import redis


class MyRedisUtils:

    def __init__(self, port=None, host=None, charset=None, password=None):
        self.redisConn = redis.Redis(host=host, port=port, charset=charset, password=password, decode_responses=True)

    def redis_str(self, key=None, value=None):
        self.redisConn.set(key, value)
        return self.redisConn.get(key)


if __name__ == '__main__':
    redis1 = MyRedisUtils(port=6379, host='121.40.162.87', charset="Utf-8", password='f12345678')
    a = redis1.redis_str("name", "张三")
    print(a)
